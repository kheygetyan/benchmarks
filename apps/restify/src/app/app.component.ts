import { ChangeDetectionStrategy, Component } from '@angular/core'

@Component({
  selector: 'benchmarks-restify',
  template: `<router-outlet></router-outlet>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
  title = 'Restify engine'
}
