import { NgModule } from '@angular/core'
import { IndexComponent } from './index.component'
import { RouterModule } from '@angular/router'
import { LayoutModule } from '../../core/layout/layout.module'

@NgModule({
  declarations: [IndexComponent],
  imports: [
    LayoutModule,
    RouterModule.forChild([
      {
        path: '',
        component: IndexComponent,
      },
    ]),
  ],
})
export class IndexModule {}
