import { NgModule } from '@angular/core'
import { ServerModule } from '@angular/platform-server'
import { AppModule } from './app.module'
import { AppComponent } from './app.component'
import { AppRoutingModule } from './app.routing.moduls'

@NgModule({
  imports: [ServerModule, AppModule, AppRoutingModule],
  bootstrap: [AppComponent],
})
export class AppServerModule {}
