import { NgModule } from '@angular/core'
import { TransferHttpCacheModule } from '@nguniversal/common'
import {
  BrowserModule,
  BrowserTransferStateModule,
} from '@angular/platform-browser'
import { RouterModule } from '@angular/router'
import { CommonModule } from '@angular/common'
import { AppComponent } from './app.component'

@NgModule({
  declarations: [AppComponent],
  imports: [
    CommonModule,
    RouterModule,
    BrowserModule.withServerTransition({ appId: 'ngExpressApp' }),
    BrowserTransferStateModule,
    TransferHttpCacheModule,
  ],
  exports: [AppComponent],
})
export class AppModule {}
