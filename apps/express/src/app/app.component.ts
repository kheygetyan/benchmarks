import { ChangeDetectionStrategy, Component } from '@angular/core'

@Component({
  selector: 'benchmarks-express',
  template: `<router-outlet></router-outlet>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
  title = 'Express engine'
}
